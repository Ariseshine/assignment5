require 'rails_helper'

feature "Post Page" do

	scenario "should show post in the database" do
		Post.create!(title: "Post Title", description: "Post Description")
		visit posts_path
		expect(page).to have_content "Post Title"
	end

end