require 'rails_helper'

feature "Create post" do 
	scenario "Successfully" do
		visit root_path
		click_on 'New Post'
		fill_in 'Title', with: 'Ruby'
		fill_in 'Description', with: 'rspec'
		click_on 'Create Post'

		click_on 'Edit'
		fill_in 'Title', with: 'Ruby'
		fill_in 'Description', with: 'rspec'
		click_on 'Update Post'

		expect(page).to have_content('Post was successfully updated.')
		expect(page).to have_content('Ruby')
		expect(page).to have_content('rspec')
		
	end

	scenario "Fail" do
		visit root_path
		post = Post.create!(title: "Ruby", description: "rspec")
		visit posts_path

		click_on 'Edit'
		fill_in 'Title', with: ''
		fill_in 'Description', with: ''
		click_on 'Update Post'

		expect(page).to have_content("Title can't be blank")
		
	end
end